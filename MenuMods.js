// ==UserScript==
// @name        Menu Mods - Yahoo Fantastic Football
// @namespace   http://ffxtra.comyr.com/
// @author      doyougl0w@gmail.com
// @include     *football.fantasysports.yahoo.com*
// @icon		http://football.fantasysports.yahoo.com/favicon.ico
// @require 	http://code.jquery.com/jquery.min.js
// @require 	http://ffxtra.comyr.com/export/jorder-1.2.js
// @require		http://ffxtra.comyr.com/export/jquery.sidecontent.js
// @resource	FantasticStyle		http://ffxtra.comyr.com/export/fantasticstyle.css
// @resource	RefreshIcon			http://ffxtra.comyr.com/images/refresh.png
// @version     .01 Alpha
// ==/UserScript==
//#///////////////////////////////////// Notes: ////////////////////////////////////#//
//
//   When in doubt, Hover over the item in question.
//
//   Please do give feedback.   doyougl0w@gmail.com
//
//#///////////////////////////////////// ToDo: /////////////////////////////////////#//
//
//	 A system for modifying the href of certain links on the page.
//	 Fantastic Module that helps manage the url parsing and keeps track of in an object.
//	 Basically a default link selector. Includes functionality to add items to the menu.
//
//   Please do give feedback.   doyougl0w@gmail.com                                                    
//
//#/////////////////////////////////////////////////////////////////////////////////#//
