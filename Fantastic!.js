// ==UserScript==
// @name        Yahoo Fantastic Football!
// @namespace   https://bitbucket.org/rizenb/
// @author      doyougl0w@gmail.com
// @include     *football.fantasysports.yahoo.com*
// @icon	https://bitbucket.org/rizenb/yahoo-fantastic-football-user-script/raw/0fa0d76c219969f2b99bfc3dc936573d1bb6f98f/Ficon.png
// @require 	http://code.jquery.com/jquery.min.js
// @require 	https://bitbucket.org/rizenb/yahoo-fantastic-football-user-script/raw/0fa0d76c219969f2b99bfc3dc936573d1bb6f98f/jorder-1.2.js
// @require	https://bitbucket.org/rizenb/yahoo-fantastic-football-user-script/raw/0fa0d76c219969f2b99bfc3dc936573d1bb6f98f/jquery.sidecontent.js
// @resource	FantasticStyle		https://bitbucket.org/rizenb/yahoo-fantastic-football-user-script/raw/0fa0d76c219969f2b99bfc3dc936573d1bb6f98f/fantasticstyle.css
// @resource	RefreshIcon		https://bitbucket.org/rizenb/yahoo-fantastic-football-user-script/raw/0fa0d76c219969f2b99bfc3dc936573d1bb6f98f/refresh.png
// @version     .77 Beta
// ==/UserScript==
//#///////////////////////////////////// Notes: ////////////////////////////////////#//
//	 Started preparing the code for some major revisions.
//   When in doubt, Hover over the item in question.
//
//	 Now exists on bitbucket, email me if you'd like to help with this scripts design.
//   Please do give feedback.   doyougl0w@gmail.com
//
//#/////////////////////////////////////////////////////////////////////////////////#//
var hasRun = false;
//# Get the saved preferences or use Default #//
var prefs = JSON.parse(localStorage.getItem('FantasticPreferences'));
if (!prefs) { 
var prefs = ({
		layout: {
			title: 'These are options that affect menus and page layout. Hover for more info.',
			loadWait: { enabled: false, title: 'Delay the display of the Fantastic Menu until after the link is clicked on each page load.' },
			slimStats: { enabled: true, title: 'This will try to slim down table widths to try and prevent horizontal scrollbars.' },
			superNotes: { enabled: true, title: 'This will resize players notes, append the player\'s game log, and add any enabled feature columns to the note popup.' },
			hideAds: { enabled: true, title: 'Removes the Ad bar and expands the width of the the main viewport.' },
			muBench: { enabled: true, title: 'Displays the bench by default on matchup pages.' },
			rightMenu: { enabled: false, title: 'Alter the Fantasy Flyout Menu on the top right.', stat1: 'GDD', stat2: 'M', ssort: 'W' },
			menuLinks: { enabled: false, title: 'Alter the Fantastic Menu links.', stat1: 'GDD', stat2: 'M', ssort: 'W' },
			weekLinks: { enabled: false, title: 'This will add the custom Tab Links to your team week menus.', stat1: 'GDD', stat2: 'M', ssort: 'W' },
			myTab: { enabled: false, title: 'This will add the custom Tab the My Team link in the header menu.', stat1: 'GDD', stat2: 'M', ssort: 'W' },
			matchupLinks: { enabled: false, title: 'This will change league links in rightMenu and menuLinks to that league\'s current matchup.'}
		},
		fpa: {
			title: 'These will display Fantasy Points Against Ranks when the url contains the value.',
			team: false,
			matchup: false,
			players: false,
			playerswatch: false,
			addplayer: false,
			dropplayer: false,
			proposetrade: false,
			playerSearch: false,
			note: false
		},
		ratings: {
			title: 'These will display Matchup Ratings when the url contains the value.',
			team: false,
			matchup: false,
			players: false,
			playerswatch: false,
			addplayer: false,
			dropplayer: false,
			proposetrade: false,
			playerSearch: false,
			note: false
		},
		targets: {
			title: 'This will display player Target Info when the url contains the value, with extra info on hover.',
			team: false,
			matchup: false,
			players: false,
			playerswatch: false,
			addplayer: false,
			dropplayer: false,
			proposetrade: false,
			playerSearch: false,
			note: false
		},
		display: {
			expanded: false,
			fpa: true,
			ratings: true,
			targets: true,
			layout: true,
			prefs: true,
			teams: false,
			cache: true,
			width: '400px',
			height: '90%'
		},
		updated: {
			fpa: 'unknown',
			ratings: 'unknown',
			targets: 'unknown',
			meta: 'unknown'
		}
	});
};
//#//////////////////////////////# Append Stylesheet #////////////////////////////////#//
var inline_CSS = document.createElement('style');
inline_CSS.type = 'text/css', inline_CSS.innerHTML = GM_getResourceText('FantasticStyle');
head.appendChild(inline_CSS);
//#//////////////////////////////# Begin Functions #////////////////////////////////#//

if (!Fantastic) {
                Fantastic = ({
                                tmp: { hasRun: hasRun, dat: {} },
                                url: function(uri) {
                                                //# Get the current URL #//
                                                var loc = window.location.toString().split('?');
                                                //# Stash Query String and Set URL to String #//
                                                if (loc.length>1) { 
                                                        var loq = loc[1], loc = loc[0];
                                                } else {
                                                        var loq = '', loc = loc[0];
                                                }
                                                this.tmp.dat.location = loc;
                                                //# Handle Query String and Save Values #//
                                                if (loq) {
                                                        if (loq.search('&') !== -1) {
                                                                var w = loq.split('&');
                                                                var muTeam = [];
                                                                for (var we in w) {
                                                                        if (w[we].search('week=') !== -1) {
                                                                                muTeam[0] = w[we].split('=')[1];
                                                                                var week = muTeam[0];
                                                                        }
                                                                        if (w[we].search('mid1=') !== -1) {
                                                                                muTeam[1] = w[we].split('=')[1];
                                                                        }
                                                                        if (w[we].search('mid2=') !== -1) {
                                                                                muTeam[2] = w[we].split('=')[1];
                                                                        }
                                                                        if (w[we].search('lid=') !== -1) {
                                                                                var currentLeague = w[we].split('=')[1];
                                                                                var currentPage = 'front';
                                                                        }
                                                                        if (w[we].search('stat1=') !== -1) {
                                                                                var currentTeamTab = w[we].split('=')[1];
                                                                        }
                                                                        if (w[we].search('stat2=') !== -1) {
                                                                                var currentTeamSubTab = w[we].split('=')[1];
                                                                        }
                                                                        if (w[we].search('ssort=') !== -1) {
                                                                                var currentsSort = w[we].split('=')[1];
                                                                        }
                                                                        if (w[we].search('tab=') !== -1) {
                                                                                var currentMuTab = w[we].split('=')[1];
                                                                        }
                                                                        if (w[we].search('pos=') !== -1) {
                                                                                var playersPos = w[we].split('=')[1];
                                                                        }
                                                                        if (w[we].search('status=') !== -1) {
                                                                                var status = w[we].split('=')[1];
                                                                        }
                                                
                                                                        
                                                                }
                                                                //
                                                                //unsafeWindow.console.log(muTeam);
                                                        } else {
                                                                // var w = loq.split('=');
                                                                // var xoo = eval('var '+w[0]+' = "'+w[1]+'"');
                                                                // unsafeWindow.console.log(tab);
                                                        }
                                                }
                                                if (loc.search('/f1/') !== -1) {
                                                        var current = loc.split('/f1/')[1].split('/');
                                                        //unsafeWindow.console.log(current);
                                                        var currentLeague = current[0].toString();
                                                        if (current[1] && current[1].match(/\d+/)) {
                                                                var currentTeam = current[1].toString();
                                                                if (current[2]) {
                                                                        var currentPage = current[2].match(/[A-Za-z]+/).toString().toLowerCase();
                                                                } else {
                                                                        var currentPage = 'team';
                                                                        //loc = loc+'/team';
                                                                }
                                                        } else {
                                                                if (current[1] && current[1].match(/[A-Za-z]+/)) {
                                                                        var currentPage = current[1].toString().toLowerCase();
                                                                } else {
                                                                        var currentPage = 'league';
                                                                        //loc = loc+'/league';
                                                                }
                                                        }
                                                        // unsafeWindow.console.log(currentLeague);
                                                        // unsafeWindow.console.log(currentTeam);
                                                        // unsafeWindow.console.log(currentPage);
                                                        // unsafeWindow.console.log(loc);
                                                }
                                                unsafeWindow.console.log(loc);
                                                
                                }
                });
} else {
   unsafeWindow.console.log('Initialized Fantastic.');
                
}

//unsafeWindow.console.log(Fantastic.tmp.dat.location);
