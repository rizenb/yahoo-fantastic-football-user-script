// ==UserScript==
// @name        Yahoo Fantastic Football!
// @namespace   https://bitbucket.org/rizenb/
// @author      doyougl0w@gmail.com
// @include     *football.fantasysports.yahoo.com*
// @icon        https://bitbucket.org/rizenb/yahoo-fantastic-football-user-script/raw/0fa0d76c219969f2b99bfc3dc936573d1bb6f98f/Ficon.png
// @require     http://code.jquery.com/jquery.min.js
// @require     https://bitbucket.org/rizenb/yahoo-fantastic-football-user-script/raw/0fa0d76c219969f2b99bfc3dc936573d1bb6f98f/jorder-1.2.js
// @run-at      document-start
// @require     https://github.com/axemclion/jquery-indexeddb/raw/master/src/jquery.indexeddb.js
// @resource    iconF   https://bitbucket.org/rizenb/yahoo-fantastic-football-user-script/raw/0fa0d76c219969f2b99bfc3dc936573d1bb6f98f/Ficon.png
// @resource    FantasticStyle      https://bitbucket.org/rizenb/yahoo-fantastic-football-user-script/raw/0fa0d76c219969f2b99bfc3dc936573d1bb6f98f/fantasticstyle.css
// @resource    RefreshIcon     https://bitbucket.org/rizenb/yahoo-fantastic-football-user-script/raw/0fa0d76c219969f2b99bfc3dc936573d1bb6f98f/refresh.png
// @version     1.06 Beta
// ==/UserScript==
//#///////////////////////////////////// Notes: ////////////////////////////////////#//
//   This is the recoded version in progress.
//   When in doubt, Hover over the item in question.
//
//   Now exists on bitbucket, email me if you'd like to help with this script's design.
//   Please do give feedback.   doyougl0w@gmail.com
//
//#/////////////////////////////////////////////////////////////////////////////////#//
//(function(window, document){
//# Don't run in frames. #//
if (window.top !== window.self) { return };
//# Get the saved preferences or use Default #//
var prefs = JSON.parse(localStorage.getItem('FantasticPreferences'));
if (!prefs) {
var prefs = ({
        layout: {
            title: 'These are features that affect menus, information, and page layout. Hover for more info.',
            loadWait: { on: false, title: 'Delay the display of the Fantastic Menu until after the link is clicked on each page load.' },
            slimStats: { on: true, title: 'This will try to slim down table widths to try and prevent horizontal scrollbars.' },
            superNotes: { on: true, title: 'This will resize players notes, append the player\'s game log, and add any on feature columns to the note popup.' },
            hideAds: { on: true, title: 'Removes the Ad bar and expands the width of the the main viewport.' },
            muBench: { on: true, title: 'Displays the bench by default on matchup pages.' },
            rightMenu: { on: false, title: 'Alter the Fantasy Flyout Menu on the top right.', stat1: 'GDD', stat2: 'M', ssort: 'W' },
            menuLinks: { on: false, title: 'Alter the Fantastic Menu links.', stat1: 'GDD', stat2: 'M', ssort: 'W' },
            weekLinks: { on: false, title: 'This will add the custom Tab Links to your team week menus.', stat1: 'GDD', stat2: 'M', ssort: 'W' },
            myTab: { on: false, title: 'This will add the custom Tab the My Team link in the header menu.', stat1: 'GDD', stat2: 'M', ssort: 'W' },
            matchupLinks: { on: false, title: 'This will change league links in rightMenu and menuLinks to that league\'s current matchup.'},
            scoreSim: { on: true, title: 'Commissioners Only. This displays a helper for balancing your league\'s scoring.'},
            newsLink: { on: true, title: 'This just removes an extra click if you were to actually click "More News" from the league page'},
            topBar: { on: true, title: 'This just removes the top bar with links to various other yahoo pages.'}
        },
        fpa: {
            title: 'These will display Fantasy Points Against Ranks when the url contains the value.',
            team: false,
            matchup: false,
            players: false,
            playerswatch: false,
            addplayer: false,
            dropplayer: false,
            proposetrade: false,
            playerSearch: false,
            note: false
        },
        ratings: {
            title: 'These will display Matchup Ratings when the url contains the value.',
            team: false,
            matchup: false,
            players: false,
            playerswatch: false,
            addplayer: false,
            dropplayer: false,
            proposetrade: false,
            playerSearch: false,
            note: false
        },
        targets: {
            title: 'This will display player Target Info when the url contains the value, with extra info on hover.',
            team: false,
            matchup: false,
            players: false,
            playerswatch: false,
            addplayer: false,
            dropplayer: false,
            proposetrade: false,
            playerSearch: false,
            note: false
        },
        display: {
            expanded: false,
            fpa: true,
            ratings: true,
            targets: true,
            layout: true,
            prefs: true,
            teams: false,
            cache: true,
            width: '400px',
            height: '90%'
        },
        updated: {
            fpa: 'unknown',
            ratings: 'unknown',
            targets: 'unknown',
            meta: 'unknown'
        }
    });
        localStorage.setItem('FantasticPreferences', JSON.stringify(prefs));
};
//#//////////////////////////////# Append Stylesheet #////////////////////////////////#//
var head = document.getElementsByTagName('head')[0];
var inline_CSS = document.createElement('style');
inline_CSS.type = 'text/css', inline_CSS.innerHTML = GM_getResourceText('FantasticStyle');
head.appendChild(inline_CSS);
//var inline_DB = document.createElement('script');
//inline_DB.type = 'text/css', inline_DB.innerHTML = GM_getResourceText('db');
//head.appendChild(inline_DB);
//#//////////////////////////////# Begin Functions #////////////////////////////////#//
if (!Fantastic) {
        var Fantastic = ({
                tmp: {hasRun:false, cache:{}, dat:{}, reg:{} },
                url: function(uri) { //# Handle the URL and create variables appropriately #//
                        var loc = (uri) ? uri.toString().split('?') : window.location.toString().split('?');
                        if (loc.length>1) {
                                var loq = loc[1], loc = loc[0];
                        } else {
                                var loq = null, loc = loc[0];
                        }
                        F.tmp.dat.location = loc;
                        if (loc.search('/f1/') !== -1) {
                                var current = loc.split('/f1/')[1].split('/');
                                F.tmp.dat.currentLeague = current[0].toString();
                                if (current[1] && current[1].match(/\d+/)) {
                                        F.tmp.dat.currentTeam = current[1].toString();
                                        if (current[2]) {
                                                F.tmp.dat.currentPage = current[2].match(/[A-Za-z]+/).toString().toLowerCase();
                                        } else { F.tmp.dat.currentPage = 'team'; }
                                } else {
                                        if (current[1] && current[1].match(/[A-Za-z]+/)) {
                                                F.tmp.dat.currentPage = current[1].toString().toLowerCase();
                                        } else { F.tmp.dat.currentPage = 'league'; }
                                }
                        }
                //# Handle Query String and Save Values #//
                        if (loq) {
                                var w = loq.split('&');
                                for (var we in w) {
                                        var lo = w[we].split('=');
                                        F.tmp.dat[lo[0]] = lo[1];
                                        if (lo[0] == 'lid') {
                                                F.tmp.dat.league = lo[1];
                                                F.tmp.dat.currentPage = 'front';
                                        }
                                }
                        }
                        return F.tmp.dat;
                },
                init: function() {
                        if (F.tmp.dat.currentPage == 'front' && prefs.layout.newsLink.on == true) {
                                $('a[href="http://sports.yahoo.com/nfl/"]').attr('href', "http://sports.yahoo.com/nfl/morenews/");
                        }
                        if (window.indexedDB) {
                                //F.db = window.indexedDB;
                        } else {
                                window.alert("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.");
                                exit();
                        }
                        if (prefs.layout.topBar.on == true) {
                                //$(document).on('click', , function(e) {
                                        //$('#yucs-top-bar').slideUp(5);
                                $('#yucs-top-bar').slideUp(0);
                                //});
                        }                       

                        
                        F.startMenu();
                        var t = localStorage.getItem('cacheMeta');
                        if (t) {
                                F.tmp.cache.meta = JSON.parse(t);
                                //unsafeWindow.console.log(F.tmp.cache);
                        } else {
                                $.when(F.buildCache()).done(function(a) {
                                        F.saveData('cacheMeta', F.tmp.cache.meta);
                                        unsafeWindow.console.log(a);
                                });
                        }
                        F.doTrue(prefs.layout);
                        F.modPage(F.tmp.dat.currentPage);
                },
                buildCache: function() { //# Generate an object containing league information. #//
                        return $.Deferred(function( dfd ){
                                F.tmp.cache.menu = F.parse.menu();
                                var m = F.tmp.cache.menu, jqs=[];
                                for (var i in m) {
                                        //if (f.jQ.maxQs>i) { //make then add
                                        jqs.push(F.jQ.make('roster-'+m[i].leagueId));
                                        var ty = jqs.length-1;
                                        F.jQ.add([{qId: 'roster-'+m[i].leagueId, funk: F.get, args: ['http://football.fantasysports.yahoo.com/f1/'+m[i].leagueId+'/starters'], cb: F.parse.rosters}]);
                                        $.when(jqs[ty]).done(function(ra){
                                                var a = ra.split('-')[1];
                                                var roo = JSON.parse(localStorage.getItem(ra));
                                                F.tmp.cache.menu[a]['rosters'] = ra;
                                                F.tmp.cache.menu[a]['leagueSize'] = roo.length;

                                        });
                                        F.jQ.next('roster-'+m[i].leagueId);
                                }
                                $.when.apply(null, jqs).done(function() {
                                        F.tmp.cache.meta = F.tmp.cache.menu;
                                        delete F.tmp.cache.q, delete F.tmp.cache.menu;
                                        //unsafeWindow.console.log(F.tmp.cache);
                                        dfd.resolve('End of All Queues');
                                });
                        }).promise();
                },
                jQ: {  q: {}, maxQs: 1, //# NEEDS maxQs and callback arguments ##/
                        /* {qId: queueName, funk: functionToRun, args: forFunktoRun, cb: funkResultsPassedHere}
                         * Each q is a $.Deferred, that resolves after all funks and their callbacks are done.
                         * var q = F.jQ.make('queueName');
                         * if (q) {
                         *      F.jQ.add({qId: queueName, funk: functionToRun, args: forFunk, cb: functionToAlterResults});
                         *      $.when(q).done(function(returns) {
                         *            //this queue is now empty, do something with return
                         *      });
                         *      F.jQ.next('queueName');
                         * }
                         */
                        make: function(name) {
                                unsafeWindow.console.log(name+' Queue Created.');
                                if (!F.tmp.cache['q']) {
                                        F.tmp.cache['q'] = {}
                                }
                                F.tmp.cache['q'][name] = [];
                                if (!F.jQ.q[name]) {
                                        F.jQ.q[name] = {funx: [], args:[], cbs: [], def: $.Deferred()};
                                        return F.jQ.q[name].def;
                                } else {
                                        return false;
                                }
                        },
                        add: function(data) {
                                for (var i=0;i<data.length;i++) {
                                        F.jQ.q[data[i].qId].funx.push(data[i].funk);
                                        F.jQ.q[data[i].qId].args.push(data[i].args);
                                        F.jQ.q[data[i].qId].cbs.push(data[i].cb);
                                }
                                return data;
                        },
                        next: function(qId) {
                                if (F.jQ.q[qId].funx.length !== 0) {
                                        var fargs = F.jQ.q[qId].args.shift();
                                        var fu = F.jQ.q[qId].funx.shift().apply(null, fargs);
                                        var cd = F.jQ.q[qId].cbs.shift();
                                        $.when(fu).done(function(r){
                                                if (cd) {
                                                        r = (typeof r !== 'Array') ? [r] : r;
                                                        $.when(cd.apply(null, r)).done(function(re){
                                                                F.saveData(qId, re);
                                                        });
                                                } else {
                                                        F.saveData(qId, r);
                                                }
                                                F.jQ.next(qId);
                                        });
                                } else {
                                        F.jQ.q[qId].def.resolve(qId);
                                        return qId;
                                }
                        },
                },
                saveData: function(i,d) { //# Stringify and Save data collections #//
                        if (typeof d == 'object') {
                                d = JSON.stringify(d);
                        }
                        var now = Date.now(), len = d.length;
                        F.tmp.reg[i] = {name: i, time: now, length: len};
                        unsafeWindow.console.log(F.tmp);
                        return localStorage.setItem(i,d);
                },
                doTrue: function (v) { //# Apply specific functions when preferences of the same name are enabled #//
                        for (var nexo in v) {
                            if (v[nexo].on == true) {
                                if (this.doPref[nexo] && typeof this.doPref[nexo] == 'function') {
                                    //unsafeWindow.console.log(nexo+' Activated.');
                                    eval("this.doPref[nexo]()");
                                }
                            }
                        }
                        F.tmp.hasRun = true;
                },
                startMenu: function(swit) { //# Handles adding menu buttons to the yahoo Page. #//
                        var y = $('#yucs>div:eq(1)');
                        y.prepend('<div class="yucs-menu_nav" id="boxF"><a id="btnFMenu" class="" title="Toggle Fantastic Menu."><span><img id="iconF" width="28" height="28" src="'+GM_getResourceURL("iconF")+'" /></span></a></div>');
                        $('#yucsHead').append('<div style="display:none;" id="Fantastic"><div id="FantasticStatus"></div><div id="prefBoxes">Fantastic Preferences</div></div>');
                        var fRef = '<span id="modRefresh" title="This will do a soft refresh of all page modifications. Useful after yahoo modifies the tables dynamically, like in player searches. It tries to do this automatically, but if it fails, hit this and it should fix."><img src="'+GM_getResourceURL("RefreshIcon")+'" /></span>';
                        $('#btnFMenu').on('click', function(e) {
                                e.preventDefault();
                                var f = $('#Fantastic');
                                if (f.is(':visible')) {
                                        f.slideUp('fast');
                                } else {
                                        f.slideDown('fast');
                                }
                                
                        });
                        //'<div id="yucs-help" class="yucs-help yucs-menu_nav">    <a role="button" data-mad="true" id="yucs-help_button" class="sp yltasis" href="javascript:void(0);" aria-label="Help" rel="nofollow">        <em class="yucs-hide yucs-menu_anchor">Help</em>    </a>    <div style="left: 948.2px; top: 83px;" tabindex="-1" role="menu" id="yucs-help_inner" class="yucs-menu yucs-hide" data-yltmenushown="/;_ylt=AwrS.p_3uOZRfxgAjAFxcJ8u">        <span style="left: 83px;" class="sp yucs-dock"></span>        <ul role="presentation" id="yuhead-help-panel">            <li role="presentation"><a id="yui_3_8_1_18_1374075100692_39" tabindex="0" role="menuitemradio" class="yucs-acct-link" href="https://us.lrd.yahoo.com/_ylt=AwrS.p_3uOZRfxgAjQFxcJ8u/SIG=161qv1c70/EXP=1375284727/**https%3A//edit.yahoo.com/mc2.0/eval_profile%3F.intl=us%26.lang=en-US%26.done=http%3A//football.fantasysports.yahoo.com/f1/34343%26amp;.src=spt%26amp;.intl=us%26amp;.lang=en-US" target="_top">Account Info</a></li><li role="presentation"><a id="yui_3_8_1_18_1374075100692_40" tabindex="-1" role="menuitemradio" href="http://us.lrd.yahoo.com/_ylt=AwrS.p_3uOZRfxgAjgFxcJ8u/SIG=11sh103u5/EXP=1375284727/**http%3A//help.yahoo.com/l/us/yahoo/sports/" rel="nofollow">Help</a></li><span class="yucs-separator" role="presentation" style="display: block;"></span><li role="presentation"><a id="yui_3_8_1_18_1374075100692_41" tabindex="-1" role="menuitemradio" href="http://us.lrd.yahoo.com/_ylt=AwrS.p_3uOZRfxgAjwFxcJ8u/SIG=125oah1rg/EXP=1375284727/**http%3A//feedback.yahoo.com/forums/169858-us-sports" rel="nofollow">Suggestions</a></li></ul></div></div>'
                        //if ($('div#yfabanner').length) {
                        //        var myhd = $('div#yfabanner').next();
                        //        if (myhd.attr('id') == 'yspteammh') {
                        //                myhd.find('span:eq(0)').prepend('<img height="20" id="iconF" src="'+GM_getResourceURL("iconF")+'" /><span id="fantasticBtn" title="Thanks for using Fantastic.  Click here to Toggle Fantastic Menu.">&nbsp;Menu</span>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;').append('&nbsp;&nbsp;-&nbsp;&nbsp;<span id="modRefresh"><img src="'+GM_getResourceURL("RefreshIcon")+'" /></span>').next().attr('title', 'This will do a soft refresh of all page modifications. Useful after yahoo modifies the tables dynamically, like in player searches. It tries to do this automatically, but if it fails, hit this and it should fix.');
                        //                myhd.parent().append('<div id="Fantastic"><div id="FantasticStatus"></div><div id="prefBoxes">Fantastic Preferences</div></div>');
                        //                myhd.find('img').css({'vertical-align': 'middle', 'cursor': 'pointer'});
                        //        } else {
                        //                myhd.find('p:eq(0)').prepend('<img height="20" id="iconF" src="'+GM_getResourceURL("iconF")+'" /><span id="fantasticBtn" title="Thanks for using Fantastic.  Click here to Toggle Fantastic Menu.">&nbsp;Menu</span>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;').append('&nbsp;&nbsp;-&nbsp;&nbsp;<span id="modRefresh"><img src="'+GM_getResourceURL("RefreshIcon")+'" /></span>').next().attr('title', 'This will do a soft refresh of all page modifications. Useful after yahoo modifies the tables dynamically, like in player searches. It tries to do this automatically, but if it fails, hit this and it should fix.');
                        //                myhd.parents('#yspcontentheader').append('<div id="Fantastic"><div id="FantasticStatus"></div><div id="prefBoxes">Fantastic Preferences</div></div>');
                        //                myhd.find('img').css({'vertical-align': 'middle', 'cursor': 'pointer'});
                        //        }
                        //}
                },
                get: function(url, data) { //# Generic cross-domain request that returns raw html #//
                        return $.Deferred(function( dfd ){
                                GM_xmlhttpRequest({
                                        method: "POST",
                                        headers: {"Content-Type": "application/x-www-form-urlencoded"},
                                        data: (data) ? data : '&',
                                        url: url,
                                        onload: function(response) {
                                                //unsafeWindow.console.log(response.responseText);
                                                dfd.resolve(response.responseText);
                                        }, onerror: function(response) {
                                                dfd.reject(response.responseText);
                                        }
                                });
                        }).promise();
                },
                parse: {
                        table: function(htmlData, slx) { //# Generic table parser #//
                                return $.Deferred(function( dfd ){
                                //var ty = prefs.types[type];
                                ////var slx = ty.slx, sub = ty.sub;
                                        //var slx = 'table.stats';
                                        var tables = $(htmlData).find(slx), ret = [], titles = [];
                                        //unsafeWindow.console.log(tables.length);
                                        tables.each(function(){
                                                var tbl = $(this), rows = [];
                                                tbl.find('tr').each(function(){
                                                        var row = $(this), td = row.find('th,td');
                                                        var tn = td.map(function(a){
                                                                return $(this).html();
                                                        }).get();
                                                        rows.push(tn);
                                                        //unsafeWindow.console.log(rows);
                                                });
                                                ret.push(rows);
                                        });
                                        if (ret.length == 1) {
                                                ret=ret[0];
                                        }
                                        //unsafeWindow.console.log(ret);
                                        dfd.resolve(ret);
                                }).promise();
                        },
                        menu: function() {
                                var els = $('.Nav-fantasy').first().find('dt'), re = {};
                                els.each(function() {
                                        var fi = $(this).find('a').eq(0);
                                        var se = $(this).next().find('a').eq(0);
                                        var both = fi.attr('href').replace('http://football.fantasysports.yahoo.com/f1/', '').split('/');
                                                if (se.text().length > 0 ) {
                                                        var leagueName = se.text();
                                                } else {
                                                        var leagueName = 'Unknown';
                                                }

                                        re[both[0]] = {leagueId: both[0], leagueName: se.text(), teamName: fi.text(), teamId: both[1]};
                                });
                                return re;
                        },
                        rosters: function(htData) {
                                return $.Deferred(function( dfd ){
                                        var ret=[];
                                        //unsafeWindow.console.log(htData);
                                        var tbls = $(htData).find('div.startingroster');
                                        if(tbls.size()>1) {
                                                tbls.each(function() {
                                                        var te = {};
                                                        var hd = $(this).find('div.header-row h5>a:eq(0)');
                                                        var n = hd.attr('href').split('/').splice(-1, 1)[0];
                                                        var na = hd.text();
                                                        var tb = $(this).find('.simpletable tbody tr td.player');
                                                        var roster = tb.map(function(z){
                                                                var a = $(this), b = a.find('a.name:eq(0)');
                                                                if (b.size()<1) {
                                                                        return {name: 'empty'};
                                                                } else {
                                                                        var yid = $(b).attr('href').split('/').splice(-1, 1)[0];
                                                                        var c = a.find('.ysf-player-team-pos:eq(0)>span').text().split(' - ');
                                                                        return {
                                                                                name: b.text(),
                                                                                yId: yid,
                                                                                pos: c[1].replace(')', ''),
                                                                                team: c[0].replace('(', '')
                                                                        }
                                                                }
                                                        }).get();
                                                        ret.push({teamId: n, teamName: na, teamRoster: roster});
                                                });
                                                //unsafeWindow.console.log(ret);
                                            dfd.resolve(ret);
                                        } else {
                                                dfd.resolve('failt');
                                        }
                                }).promise();
                        }
                },
                util: {
                },
                cache: {
                        check: function(n) {

                        },
                        targets: function() {
                                return $.Deferred(function( dfd ){
                                        var data = "position[1]=RB&position[2]=WR&position[3]=TE&week_end=21&week_start=1&year_end=2012&year_start=2012";
                                        var slx = "table.stats", url = "http://www.kffl.com/fantasy-football/targets/index.php";
                                        $.when(F.get(url, data)).done(function(ob){
                                                F.tableGrab(ob, slx).then(function(bo){
                                                        F.saveData('id', JSON.stringify(bo));
                                                        //unsafeWindow.console.log(bo);
                                                        dfd.resolve(bo);
                                                });
                                        }).fail(function(er) {
                                                unsafeWindow.console.log(er);
                                                dfd.reject(er);
                                        });
                                }).promise();
                        },
                        stats: function(statPos) {
                                return $.Deferred(function( dfd ){
                                        var stPos = (statPos) ? statPos : 'QB';
                                        var lg = F.tmp.dat.currentLeague, st = '/players?&pos='+stPos;
                                        var end = '&sort=PTS&sdir=1&status=ALL&stat1=S_PS_2013&json=1';
                                        var ur = 'http://football.fantasysports.yahoo.com/f1/'+lg+st+end;
                                        dfd.resolve(ur);
                                }).promise();
                        },

                        fpaByPos: function(id, pos) {
                                return $.Deferred(function( def ){



                                    var d = new Date();
                                    var season = d.getFullYear()-1;
                                    var earl = 'http://football.fantasysports.yahoo.com/f1/'+id+'/pointsagainst?season='+season+'&pos='+pos+'&mode=average';
                                    var abbrs = { 22: 'ari', 1: 'atl', 33: 'bal', 2: 'buf', 29: 'car', 3: 'chi', 4: 'cin', 5: 'cle', 6: 'dal', 7: 'den', 8: 'det', 9: 'gb', 34: 'hou', 11: 'ind', 30: 'jac', 12: 'kc', 15: 'mia', 16: 'min', 17: 'ne', 18: 'no', 19: 'nyg', 20: 'nyj', 13: 'oak', 21: 'phi', 23: 'pit', 24: 'sd', 26: 'sea', 25: 'sf', 14: 'stl', 27: 'tb', 10: 'ten', 28: 'was' };
                                    var ranks = [];
                                    $.when($.post(earl, function(f) {
                                        $(f).find('#statTable0').find('tr:not([class^=headerRow])').each(function() {
                                            var to = {};
                                            $(this).find('td:not(.stat)').removeClass('first').removeClass('last').removeClass('sorted').each(function() {
                                                var ba = $(this).attr('class');
                                                if (ba == 'team') {
                                                    var bb = $(this).children('a').first().attr('href').split('&');
                                                    var bc = bb.slice(-1)[0].split('=')[1];
                                                    to['ntid'] = bc;
                                                    to['abbr'] = abbrs[bc];
                                                    to['team'] = $(this).text().split(' vs ')[0];
                                                } else if (ba == 'rank') {
                                                    to['rank'] = $(this).text();
                                                    to['rankImg']  = $(this).html();
                                                } else {
                                                    to[ba] = $(this).text();
                                                }
                                            });
                                            ranks.push(to);
                                        });

                                    })).done(function() {
                                        //unsafeWindow.console.log(ranks);
                                        localStorage.setItem('pointsAgainst-'+id+'-'+pos, JSON.stringify(ranks));
                                        def.resolve();
                                    }).fail(function(d) {
                                        unsafeWindow.console.log(d);
                                    });
                                }).promise();
                        },
                },
                modPage: function(currPage) {
                        switch (currPage) {
                                case 'editstatcategories':
                                        //if (prefs.layout.scoreSim.on == true) {
                                        //        var chks = $('#editstatcategoriesform input:checked');
                                        //        F.tmp.dat.checks = chks.map(function(a,b) {
                                        //                var c = $(b).next(), d=c.next().find('input:eq(0)').val();
                                        //                return {
                                        //                        cid: this.id, chkd: this.checked, clbl: c.text(), cval: d
                                        //                };
                                        //        });
                                        //        if (typeof statCache !== 'undefined') {
                                        //                unsafeWindow.console.log(statCache);
                                        //        } else {
                                        //                var x = JSON.parse(localStorage.getItem('statCache'));
                                        //                if (x) {
                                        //                        unsafeWindow.console.log(x);
                                        //                } else {
                                        //                        F.doPref.cacheStats("O");
                                        //                }
                                        //
                                        //        }
                                        //        //unsafeWindow.console.log(joo);
                                        //}
                                break;
                                default:
                                        unsafeWindow.console.log(currPage);
                                break;
                        }
                        F.tmp.hasRun = true;
                },
                doPref: {
                        superNotes: function() {
                                //$('a.playernote').live('click', function(e){
                                //        e.stopPropagation();
                                //        var t = $(this).attr('id').split('-')[1];
                                //        if (currentPage !== 'transactions') {
                                //            var po = $(this).parents('.ysf-player-detail:eq(0)').find('span:eq(0)').text().split(' - ')[1].replace(')', '');
                                //            // //unsafeWindow.console.log(t +' | '+po);
                                //        } else {
                                //            var po = $(this).parent().text().split(' - ')[1].replace(')', '');
                                //            // // var po = $(this).parent().find('span:eq(0)').text().split(' - ')[1].replace(')', '');
                                //            // var po = $(this).prev().text().split(' - ')[1].replace(')', '');
                                //            // //alert($(this).prev().text().split(' - ')[1].replace(')', ''));
                                //        }
                                //        if (po.search(',') !== -1) { po = po.split(',')[0] };
                                //        var doAct = window.setInterval(function(){checkNote(t, doAct, po)}, 500);
                                //});
                                
                        },
                        slimStats: function() {

                        },
                        muBench: function() {
                                if (F.tmp.dat.currentPage == 'matchup') {
                                        $('#bench-toggle').text('Hide Bench [-]'); //# Show Bench automatically on matchup pages #//
                                        $('#bench').removeClass('hidden'); //# Show Bench automatically on matchup pages #//
                                }
                        },
                        scoreSim: function() {

                        },
                        hideAds: function() {
                                if (F.tmp.dat.currentPage == 'matchup') {
                                        $('#yspsub').hide(); //# Hide whitespace #//
                                        $('#yspmain').css('width', '98%'); //# Stretch to fill whitespace on matchup #//
                                }
                        }
                }
        });
        var F = Fantastic;
}
//# Bind Fantastic Events #//
F.url();
if (prefs.layout.loadWait.on == true) {
        window.addEventListener ("load", F.init, false);
} else {
        $(document).ready(function(){
                F.init();
        });
}
//})(unsafeWindow, unsafeWindow.document);