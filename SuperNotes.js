// ==UserScript==
// @name        Super Notes - Yahoo Fantastic Football
// @namespace   http://ffxtra.comyr.com/
// @author      doyougl0w@gmail.com
// @include     *football.fantasysports.yahoo.com*
// @icon		http://football.fantasysports.yahoo.com/favicon.ico
// @require 	http://code.jquery.com/jquery.min.js
// @require 	http://ffxtra.comyr.com/export/jorder-1.2.js
// @require		http://ffxtra.comyr.com/export/jquery.sidecontent.js
// @resource	FantasticStyle		http://ffxtra.comyr.com/export/fantasticstyle.css
// @resource	RefreshIcon			http://ffxtra.comyr.com/images/refresh.png
// @version     .01 Alpha
// ==/UserScript==
//#///////////////////////////////////// Notes: ////////////////////////////////////#//
//
//   When in doubt, Hover over the item in question.
//
//   Please do give feedback.   doyougl0w@gmail.com
//
//#///////////////////////////////////// ToDo: /////////////////////////////////////#//
//
//   Expanded Defensive Stats, Alternative to game log get stats from players page.
//
//   Please do give feedback.   doyougl0w@gmail.com
//
//#/////////////////////////////////////////////////////////////////////////////////#//


function checkNote(t, doAct, pos) {  //# TODO: Refactor
	var pdiv = $('div.playerinfo a.name[href*="'+t+'"]');
	if (pdiv.size()>0) {
		window.clearInterval(doAct);
		if (prefs.fpa.note == true) {
			if (pos !== 'DEF') {
				var league = loc.split('/')[4];
				//unsafeWindow.console.log(league);
				var pa = pdiv.parents('.yui3-widget:eq(0)');
				//pa.css({'top': (pa.css('top')-100)+'px'});
				var p = pa.find('.teamtable:eq(1)');
				var topp = pa.find('.teamsched:eq(0)');
				var currPos = 'pointsAgainst-'+league+'-'+pos;
				var currPts = localStorage.getItem(currPos);

				if (currPts) {
					pa.find('.yui3-widget-hd:eq(0)').css({'background-color': '#282828', 'height': '23px'}).parent().css({'width': '660px'}).find('h5').css({'vertical-align': 'middle'});
					pa.find('.yui3-ysplayernote-indicator:eq(0)').hide();
					var pep = pa.find('.yui3-widget-ft:eq(0)');
					if (pep.data('gamelog')) {
						pep.html(pep.data('gamelog'));
						$('.teamsched:eq(0)').html('').addClass('notebox').removeClass('teamsched');
					} else {
						var pUrl = pdiv.attr('href')+'/gamelog';
						GM_xmlhttpRequest({
							method: "GET",
							url: pUrl,
							onload: function(r) {
								var tab = $(r.responseText).find('.bd table');
								var tib = $(tab[1]);
								pep.html(topp.find('.teamtable:eq(0)').parent().html());
								var dat = JSON.parse(currPts);
								var myPtsAgainstData = jOrder.table(dat).index('abbr', ['abbr'], { grouped: true});
								var ths = tib.find('tr:eq(1)').find('th:gt(3)');
								pep.find('tr:eq(0) th:eq(1)').addClass('ptsHd').removeClass('date').html('Pts #');
								pep.find('tr:eq(0) th:eq(3)').after(ths);

								// pep.find('thead>tr').css({ 'position': 'relative'});
								// var poo = pep.find('table:eq(0)');
								// var tb = poo.clone();
								// pep.append(tb);
								// tb.find('thead').remove();
								// poo.find('tbody').remove();
								// //pep.parent().pref().find('div:eq(0)').after(poo);
								// pep.css({'overflow': 'hidden'});
								// pep.find('table:eq(0)').css({'position': 'relative', 'top': 0});
								// pep.find('table:eq(1)').css({'overflow': 'auto'});
								pep.find('tr:not(:has(th))').each(function(){
									var opp = $(this).find('td:eq(2)');
									var into = $(this).find('td:eq(1)');
									var eeque = $(this).index();
									var toes = tib.find('tr:not(:has(th))').css({'display':'block'}).filter(':not(:contains(bye))').eq(eeque);
									var tds = toes.find('td:gt(3)');
									$(this).find('td.status').after(tds);
									var teamAbbr = opp.text().replace('@', '').toLowerCase();
									if (teamAbbr !== '' || teamAbbr !== 'Bye') {
										var myPtsAgainstQ = myPtsAgainstData.where([{abbr: teamAbbr}]).filter(function(next) { return next !== undefined; });
										if (myPtsAgainstQ.length !== 0) {
											into.html(myPtsAgainstQ[0].rankImg);
										} else {
											unsafeWindow.console.log(teamAbbr);
											//unsafeWindow.console.log(dat);
										}
									}
								});
								pep.find('th, td').css({'text-align': 'center'});
								pep.css({'overflow': 'auto', 'height': '150px'}).find('table').addClass('teamtable').css('width', '630px');
								pep.data('gamelog', pep.html());
								//pa.find('.playernote:eq(0)').
								$('.teamsched:eq(0)').html('').removeClass('teamsched').addClass('noteBox');
								var toL = pa.find('.noteBox:eq(0)').append(pa.find('div.playernotes-bd').children('p:eq(0), p:eq(1), div:eq(0)')).css({'overflow': 'auto'});
								pa.find('.noteBox>p:eq(0)>strong').css({'display': 'inline'});
								pa.find('.yui3-widget-hd').css({'border-top-right-radius':'10px'}).parent().css({'border-top-right-radius':'10px'}).find('.second').css({'padding':'3px'});
							}
						});
					}
				} else {
					unsafeWindow.console.log('Data Update Needed. '+currPos);
				}
				//$('div.teamsched:eq(0)').prependTo('.yui3-widget-ft:eq(0)');
			}
		}
	}
}