
Latest TODO:
        Bind onbeforeunload
            cache queue or just prevent exit
	Script Updater
		Incorporate self update script
		Check on initialization
        Preference for FFT Scoring
            Choose between:
                Yahoo Default Scoring
                ONE league's scoring id only
                Each league seperately
                    requires manual input of FFToday ids
            Changes cache names to compensate
            
        Preference for Year Behaviour
	Split fetches by league and queue
                build queue with F.jQ
	Slimmer stats
		Prune columns that only have zer0s
	More news
z               Change front link to /morenews/
                    Needs live?
		Search headlines for my Players in Notes
!        Switch League Buttons
                "This page on other league", matchup, front, team, etc?
                
                
	Strength of schedule
                Get data, cache, and calculate
			Get:  http://fftoday.com/stats/fantasystats.php?o=3&PosID=10&Data=Season&LeagueID=17
                                Allow Custom FFToday League ID
                        Cache: the td content, as well as seperate object for avg SOS for positions
                                SOS-avg-RB
                        Calc: Add rows together divide 16
x	Scoring sim
		Show top 25 scorer distribution
			%qb, %wr, %rb, etc
		Apply current scoring settings and preview
			Cache top 25s in each position
			Modify f63antasy points based on stat value
	Color Columns in tables
		Configurable color
		Hover highlight col and row
		Click toggle row highlight
	Team stats on matchup page
		Last weeks avg points
		Advanced stat links
	Super links
		Modified links on page to add custom tabs to right menu and fantasy front page
!	Super Notes
		Load extra news
		Targets by Week!
                Defensive stats by week
                    http://fftoday.com/stats/playerstats.php
                        ?Season=2012&GameWeek=&PosID=99&LeagueID=17&order_by=FFPtsPerG&sort_order=DESC
                    $('span.headerstats:contains(YEAR)').closest('table').next();
                Prune null columns
                Size and Style
                    Header image
                Caching across pages
                    Data at least
                Timing and Animation
                    Make sure that the original note pops up ASAP while still starting the data load ASAP also
                    
	Advanced Stats
		Parse opponents previous weeks for useful data
			Hindsight Rating
				Number of Wrong choices / Potential choices on bench
					Average by position then average together
			Waiver Watch
				Check waivers for players who scored the most
					Weigh them vs yours
					Generate link to weigh them for your opponent
				Possibly reduced to just a link to a specific week's FA scoring
					Or links generated and added to page
                                        Or loading compare link 
				
			Historical Ratings
				Find various stats from leagues history pages about a user
					History vs manager
					Manager Win %
                                        Team vs Projections
					Manager Hindsight Rating
                                        Manager Moves
                                Might need a manager team matrix object
                                        To link a manager with teams across leagues



Prefs
    SuperNote: gameLog, targetDetail
    getUrlArgs: year, ScoreIdFFT
    tableIndex: eq(x)
    
    
Add Year to cache names only when NOT current year?

Meta Cache
    Fantastic-Meta
        League Id/Name, Team Id/Name, Positions, leagueSize, fftScorePref
SOS
    SOS-FFTodayScoringID-POS
    SOS-LeagueId
        "advanced stat" - non priority
        If history functions are enabled and data exists, incorporate that into the rating
            vs manager ratings taken into account
        Might need to do some type of interactive mode to build matrix
        Otherwise, it's predominately just the record and pts for/against.
MUR
    muRating-leagueId-teamNum
    Append to roster object instead of separate objects.
FPA
    fpAgainst-leagueId-Pos
    Append to 
TNT
    if superNotes.tntDetail = 'high': bind loading of player's utilization data into supernote gamelog
        else get TNT-Weeks 
    TNT-YTD-Year
    
Def GameLog
    http://fftoday.com/stats/playerstats.php?Season=2012&GameWeek=&PosID=99&LeagueID=17&order_by=FFPtsPerG&sort_order=DESC




Target Detail Level
    Select Option
        If High : bind loading of player's utilization data on supernote click
            Get each weeks data and format it as title
            bind hover and add column into supernote gamelog
        If Low  : get TNT-Weeks from cache
            Look up player and return row
            Append to gamelog table appropriately

FFT Scoring
    Select Option  : Set Values
        If YDefault: set cacheMeta.leagueId.fftScorePref to 17.
        If Custom  : insert textbox for numerical value, set cacheMeta.leagueId.fftScorePref to its value.
        If Advanced: loop cacheMeta to display league names, add text box for numerical value
            set cacheMeta.leagueId.fftScorePref to values
    Check value for each FFT request and apply it to the url




Priorities
    Preferences Form
        Creating the HTML (Horizontally)
        Binding the events
        Making the saving/loading functions
        
    metaCache
        Still needs to save the data that each feature uses
        Particularly Positions
        
    makeUrl
        Helper function for creating urls in queue
        year and FFTscoring
        
    Feature Data loading and Caching
        Iron out naming conventions
        Determine if metaCache needs anything else
        Implementing the cacheUpdate Queue
    
    Feature Data Processing
        Modify data however necessary to get it ready for insertion and caching
        Revise queue to apply these when appropriate
        
        
        
        
        
        
        
        
        
Ask Aaron
Features/Implementation
    Think it's worth making into custom extensions for different fantasy leagues?
Better design for:
    Data cache design
    Custom table inserter
    Custom data grabber
